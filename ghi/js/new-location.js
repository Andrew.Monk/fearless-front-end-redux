const url = "http://localhost:8000/api/states/";

document.addEventListener("DOMContentLoaded", async () => {
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    const stateSelect = document.getElementById("state");
    for (const { name, abbreviation } of data.states) {
      stateSelect.innerHTML += `<option value="${abbreviation}">${name}</option>`;
    }
  } else {
    console.log("Error");
  }

  const form = document.getElementById("create-location-form");
  form.addEventListener("submit", async (event) => {
    console.log(form);
    event.preventDefault();

    const formData = new FormData(form);

    const data = Object.fromEntries(formData);
    const locationsUrl = "http://localhost:8000/api/locations/";
    const response = await fetch(locationsUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      form.reset();
      const newLocation = await response.json();
    } else {
      console.log("Error submitting form");
    }
  });
});
